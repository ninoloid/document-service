FROM node:alpine

WORKDIR /usr/src/app

COPY . .

RUN npm i -g npm@6.14.6 && npm i

COPY . .

EXPOSE 3000

CMD ["npm", "start"]
