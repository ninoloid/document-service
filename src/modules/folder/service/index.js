const folderRepository = require('../repository')

const createOrUpdate = async ({ id, name, timestamp, is_public }, { user_id, company_id }) => {
  const filter = { id }
  const update = {
    name,
    timestamp,
    is_public,
    owner_id: user_id,
    company_id
  }

  const isFolderExists = await folderRepository.findOneFolder({ id })
  let result
  if (!isFolderExists) {
    result = await folderRepository.createFolder({ id, ...update })
    result.isCreated = true
  } else {
    const { owner_id } = isFolderExists
    if (user_id !== owner_id) {
      return {
        error: true,
        code: 401,
        message: 'User is not authorized to update this folder'
      }
    }
    result = await folderRepository.updateOneFolder(filter, update)
    result.isCreated = false
  }
  return result
}

const deleteFolder = async (folder_id, { user_id }) => {
  const isFolderExists = await folderRepository.findOneFolder({ id: folder_id })
  if (!isFolderExists) {
    return {
      error: true,
      code: 404,
      message: 'Folder not found'
    }
  }
  
  const { owner_id } = isFolderExists
  if (user_id !== owner_id) {
    return {
      error: true,
      code: 401,
      message: 'User is not authorized to delete this folder'
    }
  }

  await folderRepository.deleteFolder({ id: folder_id })
  await folderRepository.deleteDocuments({ folder_id })

  return {
    error: false,
    code: 200,
    message: 'Success delete folder'
  }
}

const getAll = async ({ user_id }) => {
  const items = await folderRepository.getAll({})
  const filteredItems = items.filter(item => {
    const { owner_id, is_public, share } = item
    return !(user_id !== owner_id && !is_public && !share.includes(user_id))
  })

  return {
    error: false,
    code: 200,
    data: filteredItems
  }
}

const getDocumentsOfAFolder = async (folder_id, { user_id }) => {
  const items = await folderRepository.getAll({ folder_id })
  const filteredItems = items.filter(item => {
    const { owner_id, is_public, share } = item
    return !(user_id !== owner_id && !is_public && !share.includes(user_id))
  })

  return {
    error: false,
    code: 200,
    data: filteredItems
  }
}

module.exports = {
  createOrUpdate,
  deleteFolder,
  getAll,
  getDocumentsOfAFolder
}
