const auth = require('../../../../middleware/auth')
const folderService = require('../../service')
const jwt = require('../../../../utils/auth/jwt')

const createOrUpdate = async (req, res) => {
  const { decoded } = jwt.verifyToken(req.headers.authorization)

  const result = await folderService.createOrUpdate(req.body, decoded)

  if (result.error) {
    return res.status(result.code).json(result)
  }

  const { id, name, type, content, timestamp, owner_id, company_id, isCreated } = result
  res.status(201).json({
    error: false,
    message: isCreated ? 'folder created' : 'folder updated',
    data: {
      id,
      name,
      type,
      content: content.blocks.length > 0 ? content : {},
      timestamp,
      owner_id,
      company_id
    }
  })
}

const deleteFolder = async (req, res) => {
  const { decoded } = jwt.verifyToken(req.headers.authorization)
  const { id } = req.body

  if (!id) {
    res.status(404).json({
      error: true,
      code: 404,
      message: 'Folder not found'
    })
  }

  const result = await folderService.deleteFolder(id, decoded)
  res.status(result.code).json(result)
}

const getAll = async (req, res) => {
  let decoded
  if (req.headers.authorization) {
    const verified = jwt.verifyToken(req.headers.authorization)
    decoded = verified.decoded 
  }

  const result = await folderService.getAll(decoded)
  res.status(result.code).json(result)
}

const getDocumentsOfAFolder = async (req, res) => {
  let decoded
  if (req.headers.authorization) {
    const verified = jwt.verifyToken(req.headers.authorization)
    decoded = verified.decoded 
  }

  const { folder_id } = req.params

  const result = await folderService.getDocumentsOfAFolder(folder_id, decoded)
  res.status(result.code).json(result)
}

const init = router => {
  router.post('/folder', auth, createOrUpdate)
  router.delete('/folder', auth, deleteFolder)
  router.get('/', getAll)
  router.get('/folder/:folder_id', getDocumentsOfAFolder)
}

module.exports = {
  init
}
