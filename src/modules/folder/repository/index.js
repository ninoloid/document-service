const ItemModel = require('../../../models/Item')

const createFolder = payload => {
  return ItemModel.create(payload)
}

const findOneFolder = match => {
  return ItemModel.findOne(match)
}

const updateOneFolder = (filter, update) => {
  return ItemModel.findOneAndUpdate(filter, update, { new: true })
}

const deleteFolder = match => {
  return ItemModel.delete(match)
}

const deleteDocuments = match => {
  return ItemModel.delete(match)
}

const getAll = filter => {
  return ItemModel.find(filter)
}

module.exports = {
  createFolder,
  findOneFolder,
  updateOneFolder,
  deleteFolder,
  deleteDocuments,
  getAll
}
