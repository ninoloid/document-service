const ItemModel = require('../../../models/Item')

const createDocument = payload => {
  return ItemModel.create(payload)
}

const findOneDocument = match => {
  return ItemModel.findOne(match)
}

const updateOneDocument = (filter, update) => {
  return ItemModel.findOneAndUpdate(filter, update, { new: true })
}

const deleteDocument = match => {
  return ItemModel.delete(match)
}


module.exports = {
  createDocument,
  findOneDocument,
  updateOneDocument,
  deleteDocument
}
