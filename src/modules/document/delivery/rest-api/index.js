const auth = require('../../../../middleware/auth')
const documentService = require('../../service')
const jwt = require('../../../../utils/auth/jwt')

const createOrUpdate = async (req, res) => {
  const { decoded } = jwt.verifyToken(req.headers.authorization)

  const result = await documentService.createOrUpdate(req.body, decoded)

  if (result.error) {
    return res.status(result.code).json(result)
  }

  const { id, name, type, folder_id, content, timestamp, owner_id, share, company_id, isCreated } = result
  res.status(201).json({
    error: false,
    message: isCreated ? 'Success set document' : 'Success update document',
    data: {
      id,
      name,
      type,
      folder_id,
      content: content.blocks.length > 0 ? content : {},
      timestamp,
      owner_id,
      share,
      company_id
    }
  })
}

const deleteDocument = async (req, res) => {
  const { decoded } = jwt.verifyToken(req.headers.authorization)
  const { id } = req.body

  if (!id) {
    res.status(404).json({
      error: true,
      code: 404,
      message: 'Document not found'
    })
  }

  const result = await documentService.deleteDocument(id, decoded)
  res.status(result.code).json(result)
}

const getDocument = async (req, res) => {
  let decoded
  if (req.headers.authorization) {
    const verified = jwt.verifyToken(req.headers.authorization)
    decoded = verified.decoded 
  }
  const { document_id } = req.params

  if (!document_id) {
    res.status(404).json({
      error: true,
      code: 404,
      message: 'Document not found'
    })
  }

  const result = await documentService.getDocument(document_id, decoded)
  res.status(result.code).json(result)
}

const init = router => {
  router.post('/document', auth, createOrUpdate)
  router.delete('/document', auth, deleteDocument)
  router.get('/document/:document_id', getDocument)
}

module.exports = {
  init
}
