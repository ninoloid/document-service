const documentRepository = require('../repository')

const createOrUpdate = async ({ id, name, timestamp, is_public, folder_id, owner_id, content, share }, { user_id, company_id }) => {
  const filter = { id }
  const update = {
    name,
    timestamp,
    is_public,
    folder_id,
    content,
    owner_id: owner_id && owner_id != 0 ? owner_id: user_id,
    share,
    company_id
  }

  const isDocumentExists = await documentRepository.findOneDocument({ id })
  let result
  if (!isDocumentExists) {
    result = await documentRepository.createDocument({ id, ...update })
    result.isCreated = true
  } else {
    const { owner_id } = isDocumentExists
    if (user_id !== owner_id) {
      return {
        error: true,
        code: 401,
        message: 'User is not authorized to update this document'
      }
    }
    result = await documentRepository.updateOneDocument(filter, update)
    result.isCreated = false
  }
  return result
}

const deleteDocument = async (document_id, { user_id }) => {
  const isDocumentExists = await documentRepository.findOneDocument({ id: document_id })
  if (!isDocumentExists) {
    return {
      error: true,
      code: 404,
      message: 'Document not found'
    }
  }
  
  const { owner_id } = isDocumentExists
  if (user_id !== owner_id) {
    return {
      error: true,
      code: 401,
      message: 'User is not authorized to delete this document'
    }
  }

  await documentRepository.deleteDocument({ id: document_id })

  return {
    error: false,
    code: 200,
    message: 'Success delete document'
  }
}

const getDocument = async (document_id, { user_id }) => {
  const isDocumentExists = await documentRepository.findOneDocument({ id: document_id })
  if (!isDocumentExists) {
    return {
      error: true,
      code: 404,
      message: 'Document not found'
    }
  }
  
  const { owner_id, is_public, share } = isDocumentExists
  if (user_id !== owner_id && !is_public && !share.includes(user_id)) {
    return {
      error: true,
      code: 401,
      message: 'User is not authorized to read this document'
    }
  }


  return {
    error: false,
    code: 200,
    message: 'Success get document',
    data: isDocumentExists
  }
}

module.exports = {
  createOrUpdate,
  deleteDocument,
  getDocument
}
