const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
  try {
    const { authorization } = req.headers

    if (!authorization) {
      throw {
        success: false,
        code: 400,
        message: 'Authorization header not found'
      }
    }

    const bearer = authorization.split(' ')
    jwt.verify(bearer[1], process.env.JWT_SECRET, err => {
      if (err) {
        throw err
      }

      next()
    })
  } catch (err) {
    res.status(401).json({
      success: false,
      code: err.code || 500,
      message: err.message || 'Something went wrong'
    })
  }
}
