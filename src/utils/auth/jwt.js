const jwt = require('jsonwebtoken')

const verifyToken = authorization => {
  try {
    if (!authorization) return {
      success: false,
      code: 400,
      message: 'Authorization header not found'
    }

    const bearer = authorization.split(' ')
    const decoded = jwt.verify(bearer[1], process.env.JWT_SECRET)
    return {
      success: true,
      code: 200,
      message: 'Success',
      decoded
    }

  } catch (err) {
    return {
      success: false,
      code: err.code || 500,
      message: err.message || 'Something went wrong'
    }
  }
}

module.exports = {
  verifyToken
}
