const mongoose = require('mongoose')
const mongoose_delete = require('mongoose-delete')

const Schema = new mongoose.Schema({
  id: {
    type: String
  },
  name: {
    type: String
  },
  type: {
    type: String
  },
  folder_id: {
    type: String
  },
  content: {
    blocks: [{
      type: {
        type: String
      },
      text: {
        type: String
      }
    }]
  },
  is_public: {
    type: Boolean,
    default: true
  },
  owner_id: {
    type: String
  },
  share: [{
    type: String
  }],
  timestamp: {
    type: String
  },
  company_id: {
    type: String
  }
}, {
  timestamps: true
});

Schema.plugin(mongoose_delete, { overrideMethods: true })
module.exports = mongoose.model('Items', Schema)
