const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const routes = require('./routes')
const mongoDB = require('./db/mongo')

try {
  if (mongoose.connection.readyState === 0) {
    mongoDB.connect()
  }
} catch (err) {
  console.log('Failed to conect to Mongo DB')
}

const app = express()
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(routes)

module.exports = app
