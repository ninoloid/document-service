const mongoose = require('mongoose')

const connect = async () => {
  try {  
    const MONGO_URL =
      process.env.NODE_ENV === 'prod' || process.env.NODE_ENV === 'staging'
        ? process.env.MONGO_DATABASE_URL
        : process.env.MONGO_DATABASE_URL_LOCAL
    await mongoose.connect(MONGO_URL, { useNewUrlParser: true })
  
    return Promise.resolve(mongoose)
  } catch(err) {
    console.log('Cannot connect to Mongo DB')
  }
}

module.exports = {
  connect
}
