const express = require('express')
const router = express.Router()
const apiRouter = express.Router()

const folderDelivery = require('../../modules/folder/delivery/rest-api')
const documentDelivery = require('../../modules/document/delivery/rest-api')

router.use('/document-service', apiRouter)
folderDelivery.init(apiRouter)
documentDelivery.init(apiRouter)

module.exports = router
